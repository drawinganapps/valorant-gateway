const axios = require("axios");
const _ = require('lodash');

const getAllAgents = (req, res) => {
    axios.get('https://playvalorant.com/page-data/en-us/agents/page-data.json').then(resp => {
        const data = _.get(resp.data, 'result.data.allContentstackAgentList.nodes.[0].agent_list');
        const agentLocalization = _.get(resp.data, 'result.pageContext.localization.agents');
        const abilitiesLocalization = _.get(resp.data, 'result.pageContext.localization.agentSpecialAbilities');
        const localization = {
            ...agentLocalization,
            ...abilitiesLocalization
        }
        return res.send({
            data,
            localization
        });
    });
}

const getAllWeapons = (req, res) => {
    axios.get('https://playvalorant.com/page-data/en-us/arsenal/page-data.json').then(resp => {
        const data = _.get(resp.data, 'result.data.allContentstackArsenal.nodes.[0].weapons_list.weapons');
        const categories = _.get(resp.data, 'result.data.allContentstackArsenal.nodes.[0].weapon_categories_dropdown.weapon_category');
        return res.send({
            data,
            categories
        });
    });
}

const getAllMaps = (req, res) => {
    axios.get('https://playvalorant.com/page-data/en-us/maps/page-data.json').then(resp => {
        const data = _.get(resp.data, 'result.data.allContentstackMaps.nodes[0].map_list');
        return res.send({
            data
        });
    });
}

module.exports = {
    getAllAgents,
    getAllWeapons,
    getAllMaps
}
